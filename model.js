'use strict';

const mongoose = require('mongoose');
const schema = require('./schema');
let mongo_uri = process.env.MONGODB_URI;
if (process.env.LOCAL_DB) {
  mongo_uri = 'mongodb://localhost/kitty';
}

// Use native promises
mongoose.Promise = global.Promise;

mongoose.connect(mongo_uri);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
	console.log('mongoose connected');
});

exports.Link = mongoose.model('Link', schema.LinkSchema);

exports.Link.on('index', function(err) {
  if (err) {
    console.error(err);
  }
});
