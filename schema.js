'use strict';

const mongoose = require('mongoose');
const url = require('./url');

var LinkSchema = mongoose.Schema({
    title: String,
    url: String,
    thumbnail: String,
    stars: Number,
    tags: [String]
}, {
  timestamps: true
});

LinkSchema.set('toJSON', {
    transform: function(doc, ret, options) {
        ret.selfRef = url.forLinkId(ret._id);
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    }
});

exports.LinkSchema = LinkSchema;
