'use strict';

const model = require('./model');

exports.all = function() {
  return model.Link.find().sort({createdAt: -1}).exec();
};

exports.find = function(id) {
  return model.Link.findById(id).exec();
};

exports.create = function(title, url, thumbnail) {
  var obj = new model.Link({
		title: title,
		url: url,
		thumbnail: thumbnail,
	});
	return obj.save();
};

exports.delete = function(id) {
	return model.Link.findByIdAndRemove(id).exec();
};

exports.update = function(id, update) {
  return model.Link.findByIdAndUpdate(id, update, {new:true}).exec();
};
