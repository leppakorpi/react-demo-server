'use strict';

const PUBLIC_URL = process.env.PUBLIC_URL || 'http://localhost:3000';

exports.forLinkId = function(id) {
  return PUBLIC_URL + '/links/' + id;
};
