'use strict';

const allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  //res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  //console.log('headers: ', JSON.stringify(req.headers));
  res.setHeader('Access-Control-Allow-Headers', req.headers['access-control-request-headers'] ||
    'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.setHeader('Access-Control-Expose-Headers', 'Location');

  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.send(200);
  } else {
    next();
  }
};

module.exports = allowCrossDomain;
