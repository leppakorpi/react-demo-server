'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const linkService = require('./link_service');
const urlLib = require('./url');
const allowCORS = require('./cors.js');

const PORT = process.env.PORT || 3000;

const app = express();

app.use(bodyParser.json());
app.use(allowCORS);

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(PORT, function () {
  console.log('Example app listening on port %d!', PORT);
});

app.get('/links', function(req, res) {
	linkService.all().then(function(obj) {
		if (!obj) {
			return res.json([]);
		}
		console.log('get %d objects', obj.length);
		res.json(obj);

	}).catch(function(reason) {
		console.error('Get failed:', reason.message || reason);
		res.status(400).end();
	});
});

app.get('/links/:id', function(req, res) {
	var id = req.params.id;

	linkService.find(id).then(function(obj) {
		console.log('get:', obj.title);
		res.json(obj.toJSON());

	}).catch(function(reason) {
		console.error('Get failed:', reason.message || reason);
		res.status(400).end();
	});
});

app.post('/links', function(req, res) {
	var title = req.body.title || '';
	var url = req.body.url || '';
	var thumbnail = req.body.thumbnail || '';

	linkService.create(title, url, thumbnail).then(function(obj) {
		console.log('created', obj.title);
		res.setHeader('Location', urlLib.forLinkId(obj._id));
		res.status(201).json(obj.toJSON());
	}).catch(function(reason) {
		console.error('Create failed:', reason.message || reason);
		res.status(400).end();
	});
});

app.put('/links/:id', function(req, res) {
	var id = req.params.id;
	var update = req.body;

	linkService.update(id, update).then(function(obj) {
		console.log('updated', obj.title);
		res.json(obj.toJSON());
	}).catch(function(reason) {
		console.error('Update failed:', reason.message || reason);
		res.status(400).end();
	});
});

app.delete('/links/:id', function(req, res) {
	var id = req.params.id;

	linkService.delete(id).then(function(obj) {
		console.log('deleted', obj.title);
		res.status(204).end();
	}).catch(function(reason) {
		console.error('Delete failed:', reason.message || reason);
		res.status(400).end();
	});
});
